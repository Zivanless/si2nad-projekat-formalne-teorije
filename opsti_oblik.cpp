#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <cstdio>
#include "Teorija.hpp"
#include "opsti.hpp"

using namespace std;


char * nadji_zajednicki(const char * s1, const char * s2)
{
	string rezultat = "";

	if (*s1 != '\0' && *s2 != '\0')
	{
		while (*s1 && *s2 && *s1 == *s2)
		{
			rezultat += *s1;
			s1++;
			s2++;
		}

	}
	else
	{
		if (*s1 == '\0')
			rezultat += s2;
		else
			rezultat += s1;
	}

	char * vrednost = new char[rezultat.length() + 1];
	copy(rezultat.begin(), rezultat.end(), vrednost);
	vrednost[rezultat.length()] = '\0';

	return vrednost;
}

char * leva_transformacija(const char* s)
{
	if (*s == '@')
	{
		char * rezultat = new char[1];
		rezultat[0] = '\0';
		return rezultat;
	}
	
	char * buffer = new char[strlen(s) + 1];
	strcpy(buffer, s);
	char * levo = strtok(buffer, "@");
	char * rezultat = new char[strlen(levo) + 1];
	strcpy(rezultat, levo);
	delete[] buffer;
	return rezultat;
}

char * desna_transformacija(const char * s)
{
	if (s[strlen(s)-1] == '@')
	{
		char * rezultat = new char[1];
		rezultat[0] = '\0';
		return rezultat;
	}
	char * buffer = new char[strlen(s) + 1];
	strcpy(buffer, s);
	char * desno;
	if (*s != '@')
	{
		strtok(buffer, "@");
		desno = strtok(nullptr, "@");
	}
	else
	{
		desno = const_cast<char *>(s)+1;
	}

	char * rezultat = new char[strlen(desno) + 1];
	strcpy(rezultat, desno);
	delete[] buffer;
	return rezultat;
}

string Teorija::opsti_oblik()
{
	if (broj_aksioma() == 0)
		return "";
		
	string resenje_levo = "";
	string resenje_desno = "";
	
	char buf[10];
	//Ovde se cuvaju zajednicki delovi svim levim i desnim transformacijama
	string levo_zajednicki = "";
	string desno_zajednicki = "";

	//Ovde se cuvaju delovi koji se dodaju za svaku transformaciju
	vector <char *> leve_transformacije;
	vector <char *> desne_transformacije;

	//Broj ponavljanja zajednickog stringa u transformacijama
	vector <int> leve_broj;
	vector <int> desne_broj;


	//Izdvajamo transformacije i postavljamo broj ponavljanja na nulu
	for (int i = 0; i < broj_transformacija(); i++)
	{
		leve_transformacije.push_back(leva_transformacija(this->dohvati_transformaciju(i).izlaz()));
		desne_transformacije.push_back(desna_transformacija(this->dohvati_transformaciju(i).izlaz()));
		leve_broj.push_back(0);
		desne_broj.push_back(0);
	}

	//Trazimo zajednicki deo svim transformacijama
	char * zajednicki_levi = new char[1];
	char * zajednicki_desni = new char[1];
	zajednicki_levi[0] = '\0';
	zajednicki_desni[0] = '\0';

	for (int i = 0; i < leve_transformacije.size(); i++)
	{
		char * zaj = nadji_zajednicki(zajednicki_levi, leve_transformacije[i]);
		if (strcmp(zaj, zajednicki_levi) != 0)
		{
			delete[] zajednicki_levi;
			zajednicki_levi = zaj;
		}

		zaj = nadji_zajednicki(zajednicki_desni, desne_transformacije[i]);
		if (strcmp(zaj, zajednicki_desni) != 0)
		{
			delete[] zajednicki_desni;
			zajednicki_desni = zaj;
		}
	}
	
	 
	string levi_str = "";
	string desni_str = "";
	bool transformacija_postoji_levo = false;
	bool transformacija_postoji_desno = false;
	
	for (int i = 0; i < leve_transformacije.size(); i++)
		if (leve_transformacije[i] != '\0')
		{
			transformacija_postoji_levo = true;
			break;
		}
		
	for (int i = 0; i < desne_transformacije.size(); i++)
		if (desne_transformacije[i] != '\0')
		{
			transformacija_postoji_desno = true;
			break;
		}
		
	//Ako postoji zajednicki deo, smanji ga na osnovni segment eg. ababab na ab
	if (transformacija_postoji_levo)
	{
	if (*zajednicki_levi)
	{
	for (int i = 1;i <= strlen(zajednicki_levi);i++)
	{
		levi_str += zajednicki_levi[i-1];

		if (strlen(zajednicki_levi)%i != 0)
		{
			continue;
		}

		if (levi_str*(strlen(zajednicki_levi)/i) == zajednicki_levi)
			break;
	}

	//Za svaku transformaciju proveri da li je konkatenacija osnovnog segmenta
	for (int i = 0; i < leve_transformacije.size(); i++)
	{//Ako nije ispisi najopstiji oblik resenja
		if (strlen(leve_transformacije[i])%levi_str.length()!= 0)
		{
			resenje_levo = vector_str(leve_transformacije);
			resenje_levo += 'L';
			break;
		}
		else if ((levi_str*(strlen(leve_transformacije[i])/levi_str.length())) != string(leve_transformacije[i]))
		{
			resenje_levo = vector_str(leve_transformacije);
			resenje_levo += 'L';
			break;
		}
		else
		{//U slucaju da jeste, upamti koliko se perioda sadrzi u transformaciji
			leve_broj[i] = strlen(leve_transformacije[i])/levi_str.length(); 
		}
		
	}


	if (resenje_levo == "")
	{//Resenje se ispisuje u obliku [osnova_levo]^(ponavljanja*transformacija+...)[aksioma1|aksioma2...][osnova_desno]^(ponavljanja*transformacija+...)
		resenje_levo = "["+levi_str+"]^(";
		for (int i = 0;i < leve_broj.size();i++)
		{
			if (leve_broj[i] != 0)
			{
				sprintf(buf,"%d",leve_broj[i]);
				resenje_levo += buf;
				resenje_levo += "*";
				resenje_levo += "K";
				sprintf(buf,"%d",i+1);
				resenje_levo += buf;
				resenje_levo += "+";
			}
		}

		resenje_levo[resenje_levo.length()-1] = ')';//Zamenimo poslednji plus zagradom
	}
	else
	{
		resenje_levo = vector_str(leve_transformacije);
		resenje_levo += 'L';
	}
	}
	}
	else
		resenje_levo = "";
		
	if (transformacija_postoji_desno)
	{
	if (*zajednicki_desni)
	{
	for (int i = 1;i <= strlen(zajednicki_desni);i++)
	{
		desni_str += zajednicki_desni[i-1];

		if (strlen(zajednicki_desni)%i != 0)
		{
			continue;
		}

		if (desni_str*(static_cast<int>(strlen(zajednicki_desni))/i) == zajednicki_desni)
			break;
	}
	
	
	for (int i = 0; i < desne_transformacije.size(); i++)
	{
		if (strlen(desne_transformacije[i])%desni_str.length()!= 0)
		{
			resenje_desno = vector_str(desne_transformacije);
			break;
		}
		else if ((desni_str*(strlen(desne_transformacije[i])/desni_str.length())) != string(desne_transformacije[i]))
		{
			resenje_desno = vector_str(desne_transformacije);
			resenje_desno += 'R';
			break;
		}
		else
		{
			desne_broj[i] = strlen(desne_transformacije[i])/desni_str.length(); 
		}
	}


	if (resenje_desno == "")
	{
		resenje_desno = "["+desni_str+"]^(";
		for (int i = 0;i < desne_broj.size();i++)
		{
			if (desne_broj[i] != 0)
			{
				sprintf(buf,"%d",desne_broj[i]);
				resenje_desno += buf;
				resenje_desno += "*";
				resenje_desno += "K";
				sprintf(buf,"%d",i+1);
				resenje_desno += buf;
				resenje_desno += "+";
			}
		}

		resenje_desno[resenje_desno.length()-1] = ')';
	}
	}
	else
	{
		resenje_desno = vector_str(desne_transformacije);
		resenje_desno += 'D';
	}
	}
	else
		resenje_desno = "";

	string resenje = resenje_levo;
	resenje += "[";

	for (int i = 0; i < broj_aksioma(); i++)
	{
		resenje += dohvati_aksiomu(i).cstr();
		resenje += "|";
	}

	resenje[resenje.length()-1] = ']';
	resenje += resenje_desno;

	while (!leve_transformacije.empty())
	{
		delete [] leve_transformacije.back();
		leve_transformacije.pop_back();
	}

	while (!desne_transformacije.empty())
	{
		delete [] desne_transformacije.back();
		desne_transformacije.pop_back();
	}

	return resenje;
}


string operator*(string & a, int b)
{
	string rezultat = "";

	for (int i = 0; i < b; i++)
	{
		rezultat += a;
	}

	return rezultat;
}

string vector_str(vector <char *> & a)
{
	string rezultat = "[";
	for(int i = 0; i < a.size(); i++)
	{
		if (a[i])
		{
			rezultat += a[i];
			if (i != a.size()-1)
				rezultat += "|";
		}
	}
	rezultat += "]^K";
	return rezultat;
}