#ifndef TEOREMA_HPP
#define TEOREMA_HPP

#include <cstring>
#include <iostream>
#include "Transformacija.hpp"

using namespace std;

class Transformacija;
//Azbuka je skup svih karatera dozvoljenih za formiranje teorema
//Azbuka je sortirani cstring koji sadrzi sva slova malog alfabeta koja su dodata u azbuku
class Azbuka
{
	char *p;
public:
	Azbuka();					//kreira se prazan skup slova
	Azbuka(const Azbuka&);		//konstruktor kopije
	Azbuka(char*);				//kreira se skup svih slova koja se bar jednom javljaju u cstringu
	Azbuka(char);				//kreira se azbuka sa jednim slovom
	~Azbuka();					//dealocira dodeljen memorijski prostor
	int dodaj(char);			//dodaje novo slovo u azbuku, vraca 1 ako je uspesno, 0 ako je vec dodato, -1 ubacivanjem nedozvoljenog karatktera
	int brisi(char);			//brise karakter iz azbuke, vraca 1 ako je uspesno, 0 ako nepostoji, -1 brisanjem nedozvoljenog karaktera
	int duzina() const;			//vraca broj elemenata azbuke
	bool element(char) const;	//vraca da li je znak element azbuke
	char operator[](int) const;	//vraca karakter sa pozicije
};



/*class Formule
{
	
};
*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Teorema je cstring
class Teorema
{
	char *p;
public:
	Teorema(const char*);				//pravi identicnu kopiju prosledjenog cstringa
	Teorema(char);				//pravi teoremu duzine 1
	Teorema(const Teorema&);	//konstruktor kopije
	~Teorema();					//dealocira dodeljen memorijski prostor
	int duzina() const;			//vraca duzinu cstringa
	bool operator==(const Teorema) const;//poredi 2 teoreme na jednakost
	bool operator!=(const Teorema) const;//poredi 2 teoreme na razlicitost
	const char operator[](int) const;//vraca karakter sa pozicije
	bool izazbuke(Azbuka&) const;	//vraca da li je teorema iz azbuke
	Teorema& operator=(const Teorema);//dodela vrednosti teorema
	friend Teorema operator*(Teorema, Transformacija);//primenjuje se transformacija na teoremu, bezuspesna transformacija vraca staru teoremu
	friend Teorema operator*(Transformacija, Teorema);//primenjuje se transformacija na teoremu
	Teorema& operator*=(Transformacija);//primenjuje transformaciju na teoremu i menja teoremu
	Teorema zameni(Teorema, Teorema, int) const;//menja teoremu1, teoremom2, na prvom pojavljivanju od indeksa
	void pisi() const;			//ispisuje teoremu na standardnom izlazu
	const char * cstr() {return p;} //vraca string iz teoreme
	friend bool operator<(const Teorema & a, const Teorema & b)
	{
		return strcmp(a.p,b.p) < 0;
	}
	bool moze(Transformacija q);
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Predstavlja ulancanu listu aksioma(aksiome su nepromenljive teoreme)
class Aksiome
{
	struct Lista
	{
		Teorema teorema;
		Lista *sled;
		
		Lista(Teorema&);
		Lista(const char*);
	};
	Lista *prvi, *posl;
public:
	Aksiome();							//kreira se prazna lista teorema
	Aksiome(Teorema&);					//kreira se lista sa jednom teoremom
	Aksiome(const char*);				//kreira se lista sa jednom teoremom
	~Aksiome();							//brise se cela lista
	void dodaj(Teorema);				//u listu se dodaje teorema
	void dodaj(const char*);			//u listu se dodaje teorema
	void brisi(Teorema&);				//iz liste se brise teorema
	int broj() const;					//vraca broj aksioma
	const Teorema operator[](int) const;//vraca aksiomu sa pozicije
	bool element(Teorema&) const;		//proverava da li je aksioma u listi
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif