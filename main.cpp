#include <iostream>
#include <stack>
#include "Teorema.hpp"
#include "Transformacija.hpp"
#include "Teorija.hpp"
#include "Igra.hpp"


using namespace std;


int main()
{
	/*cout << "Hello Zivanless World!!!" << endl;
	//testiranje igrice
	Igra ig;
	ig.pocni_nivo(2);
	for(int i = 0; i < ig.broj_aksioma(); i++)
		ig.aksiome(i).pisi(), cout << ", ";
	cout << endl;
	for(int i = 0; i < ig.broj_transformacija(); i++)
		ig.transformacije(i).pisi(), cout << ", ";
	cout << endl;
	ig.rezultat().pisi();
	cout << endl;*/

	Teorija prva;
	prva.dodaj_u_azbuku('a');
	prva.dodaj_u_azbuku('b');
	prva.dodaj_aksiomu("aaaabaaaa");
	//prva.dodaj_aksiomu("b");
	prva.dodaj_transformaciju(Transformacija("aaa@aaa","@"));
	prva.dodaj_transformaciju(Transformacija("aaaa@aaaa","@"));
	set <Teorema> k = prva.oduzimanje();
	cout << k.size();
	return 0;
}
