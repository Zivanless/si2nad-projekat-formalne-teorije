#ifndef OPSTI_HPP
#define OPSTI_HPP
#include <string>
string operator*(string & a, int b);
string vector_str(vector <char *> & a);
char * nadji_zajednicki(const char * s1, const char * s2);
char * leva_transformacija(const char* s);
char * desna_transformacija(const char * s);
#endif