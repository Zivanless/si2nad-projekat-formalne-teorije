#include "Igra.hpp"

Igra::Igra()
{
	nivo = nullptr;
	kraj = false;
}

Igra::~Igra()
{
	brisi();
}

void Igra::brisi()
{
	delete nivo;
	nivo = nullptr;
}

bool Igra::nivo_kraj() const
{
	return kraj;
}

int Igra::trenutni_nivo() const
{
	if(nivo == nullptr)
		return 0;
	return nivo->dohvati_nivo();
}

int Igra::sledeci_nivo()
{
	int k = trenutni_nivo();
	brisi();
	nivo = new Nivo(k+1);
	return k+1;
}

int Igra::prethodni_nivo()
{
	int k = trenutni_nivo();
	if(k == 1)
		return 1;
	brisi();
	nivo = new Nivo(k-1);
	return k-1;
}

int Igra::ponovi_nivo()
{
	int k = trenutni_nivo();
	brisi();
	nivo = new Nivo(k);
	return k;
}

int Igra::pocni_nivo(int i)
{
	brisi();
	if(i > 0)
		nivo = new Nivo(i);
	return i;
}

void Igra::odigraj_aksiomu(int i)
{
	if(!kraj)
	{
		kraj = (bool)nivo->odigraj_aksiomu(i);
	}
}

void Igra::odigraj_transformaciju(int i)
{
	if(!kraj)
	{
		kraj = (bool)nivo->odigraj_transformaciju(i);
	}
}

void Igra::undo()
{
	nivo->undo();
}

Teorema Igra::aksiome(int i) const
{
	return nivo->aksiome(i);
}

Transformacija Igra::transformacije(int i) const
{
	return nivo->transformacije(i);
}

Teorema Igra::trenutna() const
{
	return nivo->dohvati_stanje();
}

Teorema Igra::prethodna() const
{
	return nivo->prethodna();
}

Teorema Igra::rezultat() const
{
	return nivo->dohvati_rezultat();
}

int Igra::broj_aksioma() const
{
	return nivo->broj_aksioma();
}

int Igra::broj_transformacija() const
{
	return nivo->broj_transformacija();
}
