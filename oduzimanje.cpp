#include <set>
#include "Teorija.hpp"
#include "Teorema.hpp"

set <Teorema> Teorija::oduzimanje()
{
	set <Teorema> resenje;
	set <Teorema> pocetni;
	set <Teorema> privremeni;
	
	for (int i = 0; i < broj_aksioma(); i++)
	{
		pocetni.insert(dohvati_aksiomu(i));
	}
	
	while (!pocetni.empty())
	{
		for (set<Teorema>::iterator it= pocetni.begin(); it != pocetni.end(); ++it)
		{
			if (resenje.find(*it) == resenje.end())
			{
				resenje.insert(*it);
				for (int i = 0; i < broj_transformacija(); i++)
				{
					privremeni.insert((*it)*dohvati_transformaciju(i));
				}
			}
		}
		
		pocetni = privremeni;
		privremeni.clear();
	}
	
	return resenje;
}