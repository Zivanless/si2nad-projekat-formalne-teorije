#include "Teorema.hpp"

Aksiome::Lista::Lista(Teorema &t): teorema(t), sled(nullptr)//
{}

Aksiome::Lista::Lista(const char *c): teorema(c), sled(nullptr)//
{}

Aksiome::Aksiome(): prvi(nullptr), posl(nullptr)//
{}

Aksiome::Aksiome(Teorema &t)//
{
	prvi = new Lista(t);
	posl = prvi;
}

Aksiome::Aksiome(const char *c)//
{
	prvi = new Lista(c);
	posl = prvi;
}

Aksiome::~Aksiome()//
{
	while(prvi != nullptr)
	{
		Lista *q = prvi;
		prvi = prvi->sled;
		delete q;
	}
	prvi = posl = nullptr;
}

void Aksiome::dodaj(Teorema t)//
{
	if(element(t))
		return;
	Lista *q = new Lista(t);
	if(prvi == nullptr)
	{
		prvi = posl = q;
	}
	else
	{
		posl->sled = q;
		posl = q;
	}
}

void Aksiome::dodaj(const char *c)//
{
	dodaj(Teorema(c));
}

void Aksiome::brisi(Teorema &t)
{
	Lista *p = prvi;
	Lista *q = nullptr;
	while(p != nullptr)
	{
		if(p->teorema == t)
		{
			break;
		}
		q = p;
		p = p->sled;
	}
	if(p != nullptr)
	{
		q->sled = p->sled;
		delete p;
	}
}

int Aksiome::broj() const//
{
	int i = 0;
	Lista *p = prvi;
	while(p != nullptr)
	{
		i++;
		p = p->sled;
	}
	return i;
}

const Teorema Aksiome::operator[](int i) const//
{
	if(i < 0)
		return Teorema(nullptr);
	Lista *p = prvi;
	while(p != nullptr && i > 0)
	{
		p = p->sled;
		i--;
	}
	if(i == 0 && p!= nullptr)
		return p->teorema;
	return Teorema(nullptr);
}

bool Aksiome::element(Teorema &t) const
{
	Lista *p = prvi;
	while(p != nullptr)
	{
		if(t == p->teorema)
			return true;
		p = p->sled;
	}
	return false;
}
