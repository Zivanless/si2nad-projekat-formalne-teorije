#include "Teorema.hpp"
#include "Transformacija.hpp"

Teorema::Teorema(const char *r)//
{
	
	if(r == nullptr)
	{
		p = nullptr;
		return;
	}
	p = new char[1 + strlen(r)];
	int i = 0;
	for(i = 0; i < strlen(r) + 1; i++)
	{
		p[i] = r[i];
	}
}

Teorema::Teorema(char c)//
{
	if(c < 'a' || c > 'z')
	{
		p = new char;
		*p = '\0';
		return;
	}
	p = new char[2];
	p[0] = c;
	p[1] = '\0';
}

Teorema::Teorema(const Teorema& t)//
{
	p = new char[1 + t.duzina()];
	int i = 0;
	for(i = 0; i < t.duzina() + 1; i++)
	{
		p[i] = t.p[i];
	}
}

Teorema::~Teorema()//
{
	delete []p;
	p = nullptr;
}

int Teorema::duzina() const//
{
	return strlen(p);
}

bool Teorema::operator==(const Teorema t) const//
{
	int i = 0;
	while(t.p[i] != '\0' && p[i] != '\0' && p[i] == t.p[i])
		i++;
	if(p[i] != t.p[i])
		return false;
	return true;
}

bool Teorema::operator!=(const Teorema t) const//
{
	return !(*this == t);
}

const char Teorema::operator[](int i) const//
{
	if(i < 0 || i >= duzina())
		return '\0';
	return p[i];
}

bool Teorema::izazbuke(Azbuka &a) const//
{
	char *q = p;
	while(*q != '\0')
	{
		if(!a.element(*q))
			return false;
		q++;
	}
	return true;
}

Teorema& Teorema::operator=(const Teorema t)
{
	delete []p;
	p = nullptr;
	p = new char[1 + t.duzina()];
	int i = 0;
	for(i = 0; i < t.duzina() + 1; i++)
	{ 
		p[i] = t.p[i];
	}
	return *this;
}

int find(const char *s, char c)//vraca indeks prvog pojavljivanja elementa c
{
	char *t = strchr(const_cast<char*>(s), c);
	if(t)
		return t - s;
	return -1;
}

int count(char *s, char c)//vraca koliko puta se pojavljuje char c
{
	int i = 0;
	int j = 0;
	while(s[i] != '\0')
	{
		if(s[i] == c)
			j++;
		i++;
	}
	return j;
}

Teorema operator*(Teorema t, Transformacija q)
{
	int i;
	if((i = find(q.ulaz(), '$')) != -1)
	{
		//rotacija
		if(i == 0)
		{
			//leva
			char c = t.p[0];
			if(c == '\0')
				return t;
			i = 0;
			Teorema r = t;
			while(r.p[i] != '\0')
			{
				r.p[i] = r.p[i+1];
				i++;
			}
			r.p[i-1] = c;
			return r;
		}
		else
		{
			if(i == 1)
			{
				//desna
				if(strlen(t.p) == 0)
					return t;
				Teorema r = t;
				char c = r.p[i = strlen(r.p) - 1];
				while(i > 0)
				{
					r.p[i] = r.p[i-1];
					i--;
				}
				r.p[0] = c;
				return r;
			}
		}
	}
	else
	{
		if((i = find(q.ulaz(), '_')) != -1)
		{
			//oduzimanje
			if(count(const_cast<char*>(q.ulaz()), '_') >= strlen(t.p))
				return t;
			Teorema s = t;
			i = find(q.ulaz(), '@');
			if(i == -1)
				return t;
			s.p[s.duzina() - (strlen(q.ulaz()) - i - 1)] = '\0';
			Teorema r(s.p + i);
			return r;
		}
		else
		{
			if((i = find(q.ulaz(), '@')) != -1)
			{
				//klasicni
				int a = strlen(t.p) - strlen(q.ulaz()) + 1;
				int j = 0;
				int k = 0;
				//provera na isti oblik teoreme i ulazne transformacije
				while(j < strlen(q.ulaz()) && k < strlen(t.p))
				{
					if(q.ulaz()[j] == '@')
					{
						j++;
						k += a;
						continue;
					}
					if(q.ulaz()[j] == t.p[k])
						j++, k++;
					else
						return t;
				}
				if(j != strlen(q.ulaz()) || k != strlen(t.p))
					return t;
				int g = count(const_cast<char*>(q.izlaz()), '@');
				char *r = new char[1 + strlen(q.izlaz()) + g * (a - 1)];
				char *s = new char[1 + a];
				strncpy(s, t.p + i, a);
				s[a] = '\0';
				j = 0;
				k = 0;
				while(j < strlen(q.izlaz()))
				{
					if(q.izlaz()[j] == '@')
					{
						int e = 0;
						while(e < strlen(s))
						{
							r[k] = s[e];
							k++;
							e++;
						}
						j++;
					}
					else
					{
						r[k] = q.izlaz()[j];
						k++;
						j++;
					}
				}
				r[k] = '\0';
				delete []s;
				Teorema rt(r);
				delete []r;
				return rt;
			}
			else
			{
				//konstantni
				if(t == Teorema(const_cast<char*>(q.ulaz())))
				{
					return Teorema(const_cast<char*>(q.izlaz()));
				}
			}
		}
	}
	return t;
}

Teorema operator*(Transformacija q, Teorema t)
{
	return t * q;
}

Teorema& Teorema::operator*=(Transformacija q)
{
	return *this = *this * q;
}

void Teorema::pisi() const
{
	std::cout << p;
}

Teorema Teorema::zameni(Teorema t, Teorema q, int n) const
{
	int size;
	int sizep;
	char *s = new char[size = (sizep = duzina()) - t.duzina() + q.duzina() + 1];
	int i;
	int j;
	int pos = 0;
	int pos2 = 0;
	for(i = 0; i < size; s[i] = p[i], i++);
	i = n;
	s[size - 1] = '\0';
	j = 0;
	while(i < sizep)
	{
		if(t.p[j] != '\0')
		{
			if(p[i] == t.p[j])
				pos = j?pos:i,j++;
			else
				j = 0;
		}
		else
		{
			pos2 = i;
			break;
		}
		i++;
	}
	if(i == sizep && t.p[j] == '\0')
		pos2 = i;
	if(i == sizep && t.p[j] != '\0')
	{
		delete []s;
		return *this;
	}
	for(i = pos; q.p[i - pos] != '\0'; i++)
	{
		s[i] = q.p[i-pos];
	}
	for(j = pos2; p[j] != '\0'; i++, j++)
	{
		s[i] = p[j];
	}
	Teorema rez(s);
	delete []s;
	return rez;
}

bool Teorema::moze(Transformacija q) //vraca da li se moze transformacija q primeniti na teoremu
{
	Teorema t = *this;
	t = t * q;
	if(t == *this)
		return false;
	return true;
}

