#include "Teorema.hpp"

Azbuka::Azbuka()//
{
	p = new char[27];
	p[0] = '\0';
}

Azbuka::Azbuka(const Azbuka& az)//
{
	p = new char[27];
	for(int i = 0; i < 27; p[i] == az.p[i], i++);
}

Azbuka::Azbuka(char *r)//
{
	p = new char[27];
	p[0] = '\0';
	int i = 0;
	while(r[i] != '\0')
	{
		dodaj(r[i]);
		i++;
	}
}

Azbuka::Azbuka(char c)//
{
	p = new char[27];
	p[0] = c;
	p[1] = '\0';
}

Azbuka::~Azbuka()//
{
	delete []p;
	p = nullptr;
}

int Azbuka::dodaj(char c)//
{
	int i = 0;
	if(c < 'a' || c > 'z')
		return -1;
	while(c > p[i] && p[i] != '\0')
		i++;
	if(c == p[i])
		return 0;
	while(p[i] != '\0')
	{
		char temp = p[i];
		p[i] = c;
		c = temp;
		i++;
	}
	p[i] = c;
	p[i+1] = '\0';
	return 1;
}

int Azbuka::brisi(char c)//
{
	int i = 0;
	if(c < 'a' || c > 'z')
		return -1;
	while(c > p[i] && p[i] != '\0')
		i++;
	if(c != p[i])
		return 0;
	while(p[i]!='\0')
	{
		p[i] = p[i+1];
		i++;
	}
	return 1;
}

int Azbuka::duzina() const//
{
	int i = 0;
	while(p[i]!='\0')
		i++;
	return i;
}

bool Azbuka::element(char c) const//
{
	if(c < 'a' || c > 'z')
		return false;
	int i = 0;
	while(c > p[i] && p[i] != '\0')
		i++;
	if(c == p[i])
		return true;
}

char Azbuka::operator[](int i) const//
{
	if(i < 0 || i >= duzina())
		return '\0';
	return p[i];
}
