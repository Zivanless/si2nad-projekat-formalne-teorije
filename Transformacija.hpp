#ifndef TRANSFORMACIJA_HPP
#define TRANSFORMACIJA_HPP

#include "Teorema.hpp"


//	* je konstantan izraz, sadrzi tacno odredjena slova azbuke, obicni karakteri
//	_ je bilo koji karakter, sadrzi bilo koji karater iz azbuke, specijalan karakter
//  $ je jedan karakter, karakter rotacije, specijalan karakter
//	@ je string obrade, specijalni karakter
//
/////////////////////////////////////////////
//Podrzane su sledece transformacije, u navedenim oblicima
//	*	->	*				SPECIJALNI				bitni su i ulazni i izlazni string, samo obicni karakteri

//	@	->	*@*				DODAVANJE				
//  *@*	->	@				BRISANJE				
//  *@*	->	*@*				SUPSTITUCIJA			
//	@	->	*				KONSTANTNI				
//	*@*	->	*@*@*...*@*		KONKATENACIJA			bitni su i ulazni i izlazni string, nepredvidivo je ako se koristi @ u ulazu vise od jednom

//	@_	->	@				DESNO_ODUZIMANJE		
//  _@	->	@				LEVO_ODUZIMANJE			dozvoljeni su samo karakteri @ i _, @ tacno 1 pojavljuje, moguce je kombinovanje _, npr __@___ -> @ je dozvoljena transformacija, 
//													nepredvidivo za ostale unose

//	@$	->	$@				DESNA_ROTACIJA			
//	$@	->	@$				LEVA_ROTACIJA			bitan je samo ulazni string koji moze biti ili $@ ili @$, rezultat ne nepredvidiv za ostale unose





//klasa pojedinacnih transformacija koje se mogu primenjivati na teoreme
class Transformacija
{
	char *uulaz, *iizlaz;
	// ulaz -> izlaz
	//int tip;
public:
	Transformacija(const char*, const char*);
	Transformacija(const Transformacija&);
	~Transformacija();
	const char* ulaz() const;					//dohvata cstring ulaz
	const char* izlaz() const;					//dohvata cstring izlaz
	bool operator==(Transformacija&) const;		//vraca da li dve transformacije imaju isti oblik
	bool operator!=(Transformacija&) const;		//vraca da li dve transformacije imaju razlicite oblike
	//virtual int tip() const; 					//vraca tip transformacije(dodavanje, oduzimanje, konkatenacija...), jos uvek nije gotovo
	void pisi() const;							//ispisuje transformaciju na standardnom izlazu
};

class Lista_Transformacija
{
	struct Lista
	{
		Transformacija transformacija;
		Lista *sled;
		
		Lista(Transformacija&);					//kreira tip Liste
	};
	Lista *prvi, *posl;
public:
	Lista_Transformacija();						//kreira se prazna lista transformacija
	Lista_Transformacija(Transformacija&);		//kreira se lista transformacija sa jednim elementom
	~Lista_Transformacija();					//dealocira se memorijski prostor iskoriscen listom transformacija
	void dodaj(Transformacija);					//dodaje se transformacija u listu
	void obrisi(Transformacija&);				//brise se transformacija iz liste
	void obrisi(const char*, const char*);		//brise se transformacija ciji je ulazni oblik prvi argument, a izlazni drugi argument
	int broj() const;							//vraca broj transformacija u listi
	const Transformacija operator[](int) const;	//vraca transformaciju sa pozicije
	bool element(Transformacija&) const;		//proverava da li je transformacija u listi
};

#endif