#ifndef TEORIJA_HPP
#define TEORIJA_HPP

#include "Teorema.hpp"
#include "Transformacija.hpp"
#include <stack>
#include <set>

class Teorija
{
	Azbuka azbuka;
	//Formule...ovde
	Aksiome aksiome;
	Lista_Transformacija transformacije;
public:
	Teorija();
	Teorija(Azbuka&, Aksiome&, Lista_Transformacija&);//pravi teoriju na osnovu azbuke, aksioma i transformacija
	~Teorija();										//dealocira memorijski prostor
	
	int dodaj_u_azbuku(char);						//dodaje slovo u azbuku
	int brisi_iz_azbuke(char);						//brise slovo iz azbuke
	int duzina_azbuke() const;						//vraca duzinu azbuke
	bool iz_azbuke(char) const;						//proverava da li je slovo iz azbuke
	
	void dodaj_aksiomu(Teorema);					//u listu se dodaje teorema
	void dodaj_aksiomu(char*);						//u listu se dodaje teorema
	void brisi_aksiomu(Teorema);					//iz liste se brise teorema
	int broj_aksioma() const;						//vraca broj aksioma
	Teorema dohvati_aksiomu(int) const;				//vraca aksiomu sa pozicije
	bool iz_liste_aksioma(Teorema) const;			//vraca da li se aksioma nalazi u listi
	
	void dodaj_transformaciju(Transformacija);		//dodaje se transformacija u listu
	void obrisi_transformaciju(Transformacija&);	//brise se transformacija iz liste
	void obrisi_transformaciju(char*, char*);		//brise se transformacija ciji je ulazni oblik prvi argument, a izlazni drugi argument
	Transformacija dohvati_transformaciju(int) const;//vraca transformaciju sa pozicije
	bool iz_liste_transformacija(Transformacija) const;//vraca da li se transformacija nalazi u listi transformacija
	int broj_transformacija() const;				//vraca broj transformacija koje se nalaze u listi transformacija
	
	//Ovde dopisite vase metode za rad sa teoremama
	//To podrazumeva algoritme za trazenje, ispisivanje u prozor itd.

	//Funkcija za sada nema proveru da li su sve transformacije aditivne. Ubacicu je posle ako bude potrebna.
	//Ona vraca string tipa: [ab]^(2*K1+K2)[a|c][a]^(3*K2)
	//Ovaj string se vraca ako su sve transformacije umnosci jedne iste (odvojeno za levo i desno)
	//U suprotnom vraca implikativni oblik: [a|b]^KL[a][b|c]^KR
	//Spaghetti je kodirana, posto sam poceo da je pisem u pola noci
	//Drugi razlog za ovu monstruoznost od funkcije je to sto strtok funkcionise samo na cstringovima, koji ne omogucavaju dodavanje.
	//
	//Kako tacno funkcionise?
	//Nadje za jednu stranu najveci zajednicki podstring na pocetku svake transformacije npr. ababab
	//Zatim sazme taj string do osnovnog perioda: ab
	//Posle toga proverava koliko se puta u svakoj transformaciji ponavlja taj podstring i pamti taj broj (to je broj ispred *Kn)
	//
	//
	string opsti_oblik();

	//Funkcija za sada nema proveru da li su sve transformacije supstraktivne. Ako nisu, upada u beskonacnu petlju.
	//Mozda ubacim proveru.
	//Vraca skup posto on resava problem duplikata.
	//Na pocetku jedan skup napuni aksiomama, i obrce dok god ima elemenata u njemu
	//Iz njega se cupa element po element, proverava da li se nalazi u rezultat skupu, i ako ga nema, dodaje u rezultat.
	//Pre nego sto se doda, u privremeni skup se dodaju sve transformacije te teoreme
	//Na kraju se privremeni skup postavi za pocetni, a privremeni se obrise i nastavi da vrti
	set <Teorema> oduzimanje();
	
	bool teorema_pri_dodavanju(Teorema&);			//u slucaju da su sve transformacije iskljucivo oblika dodavanja vraca da li je teorema iz teorije, 
													//posledice su nepredvidive ukoliko se koristi za ostale oblike transformacija
	
	stack<Teorema> put_pri_dodavanju(Teorema&);		//vraca put u obliku teorema, na vrhu se nalazi pocetna aksioma, dok se na dnu nalazi karjnja teorema
													//posledice su nepredvidive ukoliko se koristi za ostale oblike transformacija
	
};

#endif