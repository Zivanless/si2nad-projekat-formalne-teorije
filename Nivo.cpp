#include "Igra.hpp"
#include <cmath>
#include <cstdlib>
#include <ctime>

Nivo::Nivo(int i = 1):stanje(""), rezultat("")
{
	//ovde se generise nivo
	srand(time(0));
	nivo = i;
	stanje = Teorema("");
	//dodavanje azbuke
	int m = 2 + log(nivo+1)/log(3) + rand()%2;
	for(int i = 0; i < m; i++)
		teorija.dodaj_u_azbuku((char)('a'+i));
	//dodavanje aksioma
	m = 1 + log(nivo+1)/log(8);
	for(int i = 0; i < m; i++)
	{
		int h = log(nivo+1)/log(2);
		int l = 1+rand()%h;
		char *c = new char[l+1];
		for(int i = 0; i < l; i++)
		{
			c[i] = rand()%teorija.duzina_azbuke() + 'a';
		}
		c[l] = '\0';
		teorija.dodaj_aksiomu(Teorema(c));
		delete []c;
	}
	//dodavanje transformacija
	m = 3 + nivo;
	bool b = false;
	while(m>0)
	{
		if(m >= 28)
		{
			m = m-8;
			konkatenacija();
		}
		if(m >= 21)
		{
			m = m-7;
			supstitucija();
		}
		if(m >= 18)
		{
			m = m-6;
			eliminacija();
		}
		if(m >= 12)
		{
			m = m-5;
			brisanje();
		}
		if(m >= 8)
		{
			m = m-4;
			specijalni();
		}
		/*if(m >= 3)
		{
			m = m-3;
			konstantni();
		}*/
		if(m >= 6 && !b)
		{
			b = true;
			m = m-3;
			rotacija();
		}
		if(m >= 0)
		{
			m = m-2;
			dodavanje();
		}
	}
	//generisanje rezultata
	rezultat = teorija.dohvati_aksiomu(rand()%broj_aksioma());
	m = 1 + log(nivo+1)/log(3) + rand()%2;
	i = 0;
	while(i < m)
	{
		Teorema pr(rezultat);
		rezultat = rezultat * teorija.dohvati_transformaciju(rand()%broj_transformacija());
		if(rezultat != pr)
			i++;
	}
}

Nivo::~Nivo()
{
	while(!stek.empty())
		stek.pop();
}

Teorema Nivo::dohvati_stanje() const
{
	return stanje;
}

void Nivo::undo()
{
	if(!stek.empty())
		stanje = stek.top(),
		stek.pop();
	else
		stanje = Teorema("");
}

Transformacija Nivo::transformacije(int i) const
{
	return teorija.dohvati_transformaciju(i);
}

Teorema Nivo::aksiome(int i) const
{
	return teorija.dohvati_aksiomu(i);
}

Teorema Nivo::dohvati_rezultat() const
{
	return rezultat;
}

int Nivo::odigraj_aksiomu(int i)
{
	if(stek.empty())
	{
		stanje = teorija.dohvati_aksiomu(i);
		if(stanje == rezultat)
			return 1;
	}
	return 0;
}

int Nivo::odigraj_transformaciju(int i)
{
	if(stanje != Teorema(""))
	{
		Teorema t(stanje);
		stanje = stanje * teorija.dohvati_transformaciju(i);
		if(stanje != t)
		{
			stek.push(t);
			if(stanje == rezultat)
				return 1;
		}
	}
	return 0;
}

int Nivo::dohvati_nivo() const
{
	return nivo;
}

int Nivo::broj_aksioma() const
{
	return teorija.broj_aksioma();
}

int Nivo::broj_transformacija() const
{
	return teorija.broj_transformacija();
}

Teorema Nivo::prethodna() const
{
	if(stek.empty())
	{
		return Teorema("");
	}
	return stek.top();
}

void Nivo::dodavanje()
{
	int m = 1 + log(nivo+1)/log(3) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[m+1] = '\0';
	teorija.dodaj_transformaciju(Transformacija("@", c));
	delete []c;
}

void Nivo::brisanje()
{
	int m = 1 + log(nivo+1)/log(5) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[m+1] = '\0';
	teorija.dodaj_transformaciju(Transformacija(c, "@"));
	delete []c;
}

void Nivo::specijalni()
{
	int m = 1 + log(nivo+1)/log(3) + rand()%2;
	int k = rand()%(m-1) + 1;
	char *c = new char[k+1];
	char *d = new char[m-k+1];
	for(int i = 0; i < k; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[k]='\0';
	for(int i = k; i <= m; i++)
	{
		d[i-k] = rand()%teorija.duzina_azbuke() + 'a';
	}
	d[m-k+1] = '\0';
	teorija.dodaj_transformaciju(Transformacija(c, d));
	delete []c;
	delete []d;
}

void Nivo::konstantni()
{
	int m = 1 + log(nivo+1)/log(5) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+1];
	for(int i = 0; i < m; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[m] = '\0';
	teorija.dodaj_transformaciju(Transformacija("@", c));
	delete []c;
}

void Nivo::rotacija()
{
	int m = rand()%2;
	char *c = new char[3];
	char *d = new char[3];
	c[2] = d[2] = '\0';
	c[m] = d[1-m] = '@';
	c[1-m] = d[m] = '$';
	teorija.dodaj_transformaciju(Transformacija(d, c));
	delete []c;
	delete []d;
}

void Nivo::eliminacija()
{
	int m = 1 + log(nivo+1)/log(6) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		c[i] = '_';
	}
	c[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		c[i] = '_';
	}
	c[m+1] = '\0';
	teorija.dodaj_transformaciju(Transformacija(c, "@"));
	delete []c;
}

void Nivo::supstitucija()
{
	int m = 1 + log(nivo+1)/log(5) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[m+1] = '\0';
	m = 1 + log(nivo+1)/log(5) + rand()%2;
	k = rand()%m;
	char *d = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		d[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	d[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		d[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	d[m+1] = '\0';
	teorija.dodaj_transformaciju(Transformacija(d, c));
	delete []c;
	delete []d;
}

void Nivo::konkatenacija()
{
	int m = 1 + log(nivo+1)/log(5) + rand()%2;
	int k = rand()%m;
	char *c = new char[m+2];
	for(int i = 0; i < k; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[k] = '@';
	for(int i = k+1; i < m+1; i++)
	{
		c[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	c[m+1] = '\0';
	m = 1 + log(nivo+1)/log(3) + rand()%2;
	k = rand()%m;
	int n = 1+rand()%(nivo/20);
	char *d = new char[m+1];
	for(int i = 0; i < m; i++)
	{
		d[i] = rand()%teorija.duzina_azbuke() + 'a';
	}
	for(int i = 0; i < n; i++)
	{
		d[rand()%m] = '@';
	}
	d[m] = '\0';
	teorija.dodaj_transformaciju(Transformacija(c, d));
	delete []c;
	delete []d;
}
