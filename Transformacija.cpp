#include "Transformacija.hpp"

Transformacija::Transformacija(const char *a, const char *b)//
{
	int n = strlen(a);
	int m = strlen(b);
	uulaz = new char[1 + n];
	iizlaz = new char[1 + m];
	for(int i = 0; i < n; uulaz[i] = a[i], i++);
	for(int i = 0; i < m; iizlaz[i] = b[i], i++);
	uulaz[n] = '\0';
	iizlaz[m] = '\0';
}

Transformacija::Transformacija(const Transformacija& t)
{
	int n = strlen(t.uulaz);
	int m = strlen(t.iizlaz);
	uulaz = new char[1 + n];
	iizlaz = new char[1 + m];
	for(int i = 0; i < n; uulaz[i] = t.uulaz[i], i++);
	for(int i = 0; i < m; iizlaz[i] = t.iizlaz[i], i++);
	uulaz[n] = '\0';
	iizlaz[m] = '\0';
}

Transformacija::~Transformacija()//
{
	delete []uulaz;
	delete []iizlaz;
	uulaz = iizlaz = nullptr;
}

const char* Transformacija::ulaz() const//
{
	return uulaz;
}

const char* Transformacija::izlaz() const//
{
	return iizlaz;
}

bool Transformacija::operator==(Transformacija &t) const
{
	return (iizlaz == t.iizlaz && uulaz == t.uulaz);
}

bool Transformacija::operator!=(Transformacija &t) const
{
	return !(*this == t);
}

void Transformacija::pisi() const
{
	cout << uulaz << "->" << iizlaz;
}
