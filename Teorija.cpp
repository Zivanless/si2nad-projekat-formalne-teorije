#include "Teorija.hpp"
#include "Teorema.hpp"
#include "Transformacija.hpp"
#include <iostream>

Teorija::Teorija()
{}

Teorija::Teorija(Azbuka& az, Aksiome& ak, Lista_Transformacija& trans)
{
	int i = 0;
	int n = az.duzina();
	for(i = 0; i < n; i++)
		azbuka.dodaj(az[i]);
	n = ak.broj();
	for(i = 0; i < n; i++)
		aksiome.dodaj(ak[i]);
	n = trans.broj();
	i = 0;
	for(i = 0; i < n; i++)
		transformacije.dodaj(trans[i]);
}

Teorija::~Teorija()
{}

int Teorija::dodaj_u_azbuku(char c)
{
	return azbuka.dodaj(c);
}

int Teorija::brisi_iz_azbuke(char c)
{
	return azbuka.brisi(c);
}

int Teorija::duzina_azbuke() const
{
	return azbuka.duzina();
}

bool Teorija::iz_azbuke(char c) const
{
	return azbuka.element(c);
}

void Teorija::dodaj_aksiomu(Teorema t)
{
	aksiome.dodaj(t);
}

void Teorija::dodaj_aksiomu(char *c)
{
	aksiome.dodaj(c);
}

void Teorija::brisi_aksiomu(Teorema t)
{
	aksiome.brisi(t);
}

int Teorija::broj_aksioma() const
{
	return aksiome.broj();
}
	
void Teorija::dodaj_transformaciju(Transformacija t)
{
	transformacije.dodaj(t);
}

void Teorija::obrisi_transformaciju(Transformacija &t)
{
	transformacije.obrisi(t);
}

void Teorija::obrisi_transformaciju(char *c, char *d)
{
	transformacije.obrisi(c, d);
}

Teorema Teorija::dohvati_aksiomu(int i) const
{
	return aksiome[i];
}

Transformacija Teorija::dohvati_transformaciju(int i) const
{
	return transformacije[i];
}

bool Teorija::iz_liste_aksioma(Teorema t) const
{
	return aksiome.element(t);
}

bool Teorija::iz_liste_transformacija(Transformacija t) const
{
	return transformacije.element(t);
}

int Teorija::broj_transformacija() const
{
	return transformacije.broj();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int find(Teorema t, char c)
{
	int i = 0;
	while(t[i] != '\0')
	{
		if(t[i] == c)
			return i;
		i++;
	}
	return -1;
}

bool Teorija::teorema_pri_dodavanju(Teorema &t)
{
	//trazenje aksioma i stavljanje na stek
	stack<Teorema> stack;
	int n = aksiome.broj();
	int i;
	for(i = 0; i < n; i++)
	{
		int j = 0;
		int duz = t.duzina();
		while(j < duz)
		{
			Teorema q(t);
			q = t.zameni(aksiome[i], Teorema("@"), j);
			j++;
			if(q == t)
				break;
			if((!stack.empty() && stack.top() != q) || stack.empty())
			{
				stack.push(q);
				j = find(q, '@') + 1;
			}
		}
	}
	//formiranje teorema i stavljanje na stack
	n = transformacije.broj();
	while(!stack.empty())
	{
		Teorema q = stack.top();
		stack.pop();
		if(q == Teorema("@"))
			return true;
		i = 0;
		for(i = 0; i < n; i++)
		{
			Teorema r = q;
			r = q.zameni(Teorema(transformacije[i].izlaz()), Teorema(transformacije[i].ulaz()), 0);
			if(r != q)
			{
				stack.push(r);
			}
		}
	}
	return false;
}

stack<Teorema> Teorija::put_pri_dodavanju(Teorema &t)
{
	stack<Teorema> stek, pom;
	stack<int> sifra;
	int n = aksiome.broj();
	int m = transformacije.broj();
	int i;
	int duz = t.duzina();
	for(i = 0; i < n; i++)
	{
		int j = 0;
		while(j < duz)
		{
			Teorema q(t);
			q = t.zameni(aksiome[i], Teorema("@"), j);
			j++;
			if(q == t)
				break;
			if((!stek.empty() && stek.top() != q) || stek.empty())
			{
				stek.push(q);
				sifra.push(-1);
				j = find(q, '@') + 1;
				while(!stek.empty())
				{
					Teorema q = stek.top();
					stek.pop();
					int qr = sifra.top();
					sifra.pop();
					qr++;
					if(q == Teorema("@"))
					{
						//stack<Teorema> pom;
						Teorema aks = aksiome[i];
						stek.push(Teorema("@"));
						while(!stek.empty())
						{
							Teorema r = stek.top();
							stek.pop();
							pom.push(r.zameni(Teorema("@"), aks, 0));
						}
						while(!pom.empty())
						{
							stek.push(pom.top());
							pom.pop();
						}
						return stek;
					}
					if(qr < m)
					{
						stek.push(q);
						sifra.push(qr);
						Teorema r = q;
						r = q.zameni(Teorema(transformacije[qr].izlaz()), Teorema(transformacije[qr].ulaz()), 0);
						if(r != q)
						{
							stek.push(r);
							sifra.push(-1);
						}
					}
				}
			}
		}
	}
	stack<Teorema> rez;
	return rez;
}
