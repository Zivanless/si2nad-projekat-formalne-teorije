#include "Transformacija.hpp"

Lista_Transformacija::Lista::Lista(Transformacija &t): transformacija(t), sled(nullptr)//
{}

Lista_Transformacija::Lista_Transformacija(): prvi(nullptr), posl(nullptr)//
{}

Lista_Transformacija::Lista_Transformacija(Transformacija &t)//
{
	posl = prvi = new Lista(t);
}

Lista_Transformacija::~Lista_Transformacija()//
{
	while(prvi != nullptr)
	{
		Lista *q = prvi;
		prvi = prvi->sled;
		delete q;
	}
	prvi = posl = nullptr;
}

void Lista_Transformacija::dodaj(Transformacija t)//
{
	Lista *q = new Lista(t);
	if(prvi != nullptr)
	{
		posl->sled = q;
		posl = q;
	}
	else
	{
		prvi = posl = q;
	}
}

void Lista_Transformacija::obrisi(Transformacija &t)//
{
	Lista *q = prvi;
	Lista *r = nullptr;
	while(q != nullptr)
	{
		if(q->transformacija == t)
			break;
		r = q;
		q = q->sled;
	}
	if(q == nullptr)
		return;
	if(r == nullptr)
	{
		q = prvi;
		prvi = prvi->sled;
		delete q;
		q = nullptr;
		if(prvi == nullptr)
			posl = nullptr;
		return;
	}
	if(q->sled == nullptr)
	{
		posl = r;
		delete q;
		q = nullptr;
		posl->sled = nullptr;
		return;
	}
	r->sled = q->sled;
	delete q;
}

void Lista_Transformacija::obrisi(const char *u, const char *i)//
{
	Transformacija *t = new Transformacija(u, i);
	obrisi(*t);
}

int Lista_Transformacija::broj() const//
{
	int i = 0;
	Lista *p = prvi;
	while(p != nullptr)
	{
		i++;
		p = p->sled;
	}
	return i;
}

const Transformacija Lista_Transformacija::operator[](int i) const//
{
	if(i < 0)
		return Transformacija("", "");
	int n = broj();
	if(i >= n)
		return Transformacija("", "");
	Lista *p = prvi;
	for(int j = 0; j < i; j++)
	{
		p = p->sled;
	}
	return p->transformacija;
}

bool Lista_Transformacija::element(Transformacija &t) const
{
	Lista *p = prvi;
	while(p != nullptr)
	{
		if(t == p->transformacija)
			return true;
		p = p->sled;
	}
	return false;
}
