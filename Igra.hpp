#ifndef IGRA_HPP
#define IGRA_HPP

#include "Teorema.hpp"
#include <stack>
#include "Teorija.hpp"
#include "Transformacija.hpp"

class Nivo
{
	Teorija teorija;
	Teorema stanje;
	Teorema rezultat;
	int nivo;
	stack<Teorema> stek;
public:
	Nivo(int);												//Generise dati nivo
	~Nivo();												//Vrsi destrukciju svih objekata
	Teorema dohvati_stanje() const;					//Rezultat je const reference na teoremu trenuntnog stanja
	Teorema prethodna() const;								//Rezultat je prethodna teorema
	void undo();											//Vraca trenutno stanje na prethodno
	Transformacija transformacije(int) const;		//vraca referencu na transformaciju na datoj poziciji u teoriji
	Teorema aksiome(int) const;						//vraca referencu na aksiome na datoj poziciji u teoriji
	Teorema dohvati_rezultat() const;						//Vraca teoremu koju je potrebno dobiti za kraj nivoa
	int odigraj_aksiomu(int);								//menja stanje u sledece stanje za aksiomu ciji je redni broj zadat, vraca 1 ako je kraj, 0 u suprotnom
	int odigraj_transformaciju(int);						//menja stanje u sledece stanje za zadati redni broj transformacije, vraca 1 ako je kraj, 0 u suprotnom
	int dohvati_nivo() const;										//vraca broj nivoa koji je generisan
	int broj_aksioma() const;								//vraca ukupan broj aksioma trenutnog nivoa
	int broj_transformacija() const;						//vraca ukupan broj transformacija trenutnog nivoa
private:
	void dodavanje();
	void rotacija();
	void konstantni();
	void specijalni();
	void brisanje();
	void eliminacija();
	void supstitucija();
	void konkatenacija();
};

class Igra
{
	Nivo *nivo;
	bool kraj;
public:
	Igra();													//kreira objekat tipa igra, potrebno samo za pocetak igre ili ako se igraju vise igara paralelno
	~Igra();												//brise objekat tipa igra
	void brisi();											//prazni(brise) objekat tipa igra
	
	//Kontrolne metode
	void pocni_igru();										//pocinje se od prvog nivoa
	int sledeci_nivo();										//brise sadasnji nivo i generise sledeci
	int prethodni_nivo();									//brise sadasnji nivo i generise prethodni, ukoliko je 1. nivo, nista se ne desava
	int ponovi_nivo();										//brise sadasnji nivo i generise novi nivo sa istim rednim brojem
	int pocni_nivo(int);									//brise sadasnji nivo i pocinje nivo sa ciji je argument redni broj nivoa, podrazumeva se prvi nivo
	int trenutni_nivo() const;								//Vraca trenutni nivo
	bool nivo_kraj() const;									//metoda koja proverava da li je kraj nivoa
	
	//Metode promene stanja
	void odigraj_aksiomu(int);								//ukoliko korisnik trazi da se odigra aksioma koja se nalazi na i-toj poziciji u teoriji
	void odigraj_transformaciju(int);						//ukoliko korisnik trazi da se odigra transformacija koja se nalazi na i-toj poziciji u teoriji
	void undo();											//Vraca na prethodno stanje
	
	//Metode informacija
	int broj_aksioma() const;						//Vraca broj aksioma
	int broj_transformacija() const;				//Vraca broj transformacija
	Teorema aksiome(int) const;						//vraca referencu na aksiome na datoj poziciji u teoriji
	Transformacija transformacije(int) const;		//vraca referencu na transformaciju na datoj poziciji u teoriji
	Teorema trenutna() const;						//Rezultat je const reference na teoremu trenuntne teoreme nivoa
	Teorema prethodna() const;						//Rezultat je prethodna teorema
	Teorema rezultat() const;						//vraca cilj igre
	
};


#endif